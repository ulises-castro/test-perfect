import React, { Component } from 'react';
import Deck from "react-poker";
import axios from 'axios';

import './App.css';


class App extends Component {
  
  let queensCounter = 0;
  let cardsData = [];
  
  const cardsBoard = Array(51).fill(0);
  
  constructor(props) {
    super(props);

    this.state = {
      board: [],
      cards: [],
    };
  }
  
  async componentWillMount() {
    const { data } = await axios.get('https://deckofcardsapi.com/api/deck/new/draw/?count=52');
    cardsData = data.data;
  }
  
  drawCard = () => {
    let { board, cards } = this.state;
    const suits = ["d", "c", "h", "s"];
    const queensCode = ["QD", "QC", "QH", "QS"];
    const ranks = [ "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];

    if (queensCounter === 2) {
      console.log('TERMINO!', cardsBoard);
      // Draw cards
      return ;
    }
    
    const currentCard = cardsData.pop();
    const cardRank = currentCard.code.charAt(0)
    const cardSuit = currentCard.code.charAt(1).toLowerCase();
    
    const suitPosition = suits.indexOf(cardSuit) * 13;
    const cardPosition = ranks.indexOf(cardRank) + suitPosition;
    cardsBoard[cardPosition] = 1;
    
    console.log(cardsBoard, cardPosition, cardRank);
    const cardCode = cardRank + cardSuit;
    board.concat(cardCode);

    if (queensCode.indexOf(currentCard.code) !== -1) {
      queensCounter += 1;
    }
    
    this.setState(Object.assign({}, {
      board: board.concat(cardCode),
    }));
  }

  render() {
    return (
      <div className="App">
        <div>
          <button onClick={this.drawCard}>
            Ordenar
          </button>
        </div>
        <div className="deck-container">
          <Deck
            board={this.state.board} 
            boardXoffset={500}
            boardYoffset={200}
            size={200}
          />
        </div>
      </div>
    );
  }
}

export default App;
